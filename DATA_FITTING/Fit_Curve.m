function [X] = Fit_Curve(x, order, Ts)
%This function performs a 0th order curve fit

n = length(x);
M = order + 1;
N = order + 1;

A = zeros(M, N);
B = zeros(M, 1);
X = zeros(M, 1);

if(order == 0)
    for k =1:n
        X(1) = X(1) + x(k);
    end
    
X(1) = X(1)/n;
sprintf('a0 = %3.5f', X(1))

elseif(order==1)
for i=1:M
    for j=1:N
        % A(1x1) always = n
        if((i == 1) && (j == 1))
            A(i, j) = n;
        end
        for k=1:n
            % Calculates diagonal, and above and to the right of diagonal,
            % except for A(1x1)
            if((j >= i) && (j ~= 1))
                A(i, j) = A(i, j) + ((k-1)*Ts)^(i + j - 2); 
            % Calculates everything below and to the left of the diagonal
            % by using the fact that the A matrix is symmetric about the
            % Diagonal.
            elseif((i > j) && (i ~= 1))
                A(i, j) = A(j, i); 
            end
            % Calculates B(1x1)
            if((i == 1) && (j == 1))
                B(i, j) = B(i, j) + x(k);
            % Calculates all other rows of B(ix1)
            elseif((i~=1) && (j==1))
                B(i, j) = B(i, j) + (((k-1)*Ts)^(i-1))*x(k);
            end
        end
    end
end  

X = A\B;
sprintf('a0 = %3.5f\n a1 = %3.5f', X(1), X(2))

elseif(order==2)
for i=1:M
    for j=1:N
        % A(1x1) always = n
        if((i == 1) && (j == 1))
            A(i, j) = n;
        end
        for k=1:n
            % Calculates diagonal, and above and to the right of diagonal,
            % except for A(1x1)
            if((j >= i) && (j ~= 1))
                A(i, j) = A(i, j) + ((k-1)*Ts)^(i + j - 2); 
            % Calculates everything below and to the left of the diagonal
            % by using the fact that the A matrix is symmetric about the
            % Diagonal.
            elseif((i > j) && (i ~= 1))
                A(i, j) = A(j, i); 
            end
            % Calculates B(1x1)
            if((i == 1) && (j == 1))
                B(i, j) = B(i, j) + x(k);
            % Calculates all other rows of B(ix1)
            elseif((i~=1) && (j==1))
                B(i, j) = B(i, j) + (((k-1)*Ts)^(i-1))*x(k);
            end
        end
    end
end  

X = A\B;
sprintf('a0 = %3.5f\n a1 = %3.5f\n a2 = %3.5f', X(1), X(2), X(3))

elseif(order==3)
for i=1:M
    for j=1:N
        % A(1x1) always = n
        if((i == 1) && (j == 1))
            A(i, j) = n;
        end
        for k=1:n
            % Calculates diagonal, and above and to the right of diagonal,
            % except for A(1x1)
            if((j >= i) && (j ~= 1))
                A(i, j) = A(i, j) + ((k-1)*Ts)^(i + j - 2); 
            % Calculates everything below and to the left of the diagonal
            % by using the fact that the A matrix is symmetric about the
            % Diagonal.
            elseif((i > j) && (i ~= 1))
                A(i, j) = A(j, i); 
            end
            % Calculates B(1x1)
            if((i == 1) && (j == 1))
                B(i, j) = B(i, j) + x(k);
            % Calculates all other rows of B(ix1)
            elseif((i~=1) && (j==1))
                B(i, j) = B(i, j) + (((k-1)*Ts)^(i-1))*x(k);
            end
        end
    end
end  

X = A\B;
sprintf('a0 = %3.5f\n a1 = %3.5f\n a2 = %3.5f\n a3 = %3.5f', X(1), X(2), X(3), X(4))

elseif(order==4)
for i=1:M
    for j=1:N
        % A(1x1) always = n
        if((i == 1) && (j == 1))
            A(i, j) = n;
        end
        for k=1:n
            % Calculates diagonal, and above and to the right of diagonal,
            % except for A(1x1)
            if((j >= i) && (j ~= 1))
                A(i, j) = A(i, j) + ((k-1)*Ts)^(i + j - 2); 
            % Calculates everything below and to the left of the diagonal
            % by using the fact that the A matrix is symmetric about the
            % Diagonal.
            elseif((i > j) && (i ~= 1))
                A(i, j) = A(j, i); 
            end
            % Calculates B(1x1)
            if((i == 1) && (j == 1))
                B(i, j) = B(i, j) + x(k);
            % Calculates all other rows of B(ix1)
            elseif((i~=1) && (j==1))
                B(i, j) = B(i, j) + (((k-1)*Ts)^(i-1))*x(k);
            end
        end
    end
end  

X = A\B;
sprintf('a0 = %3.5f\n a1 = %3.5f\n a2 = %3.5f\n a3 = %3.5f\n a4 = %3.5f', X(1), X(2), X(3), X(4), X(5))
    elseif(order==5)
for i=1:M
    for j=1:N
        % A(1x1) always = n
        if((i == 1) && (j == 1))
            A(i, j) = n;
        end
        for k=1:n
            % Calculates diagonal, and above and to the right of diagonal,
            % except for A(1x1)
            if((j >= i) && (j ~= 1))
                A(i, j) = A(i, j) + ((k-1)*Ts)^(i + j - 2); 
            % Calculates everything below and to the left of the diagonal
            % by using the fact that the A matrix is symmetric about the
            % Diagonal.
            elseif((i > j) && (i ~= 1))
                A(i, j) = A(j, i); 
            end
            % Calculates B(1x1)
            if((i == 1) && (j == 1))
                B(i, j) = B(i, j) + x(k);
            % Calculates all other rows of B(ix1)
            elseif((i~=1) && (j==1))
                B(i, j) = B(i, j) + (((k-1)*Ts)^(i-1))*x(k);
            end
        end
    end
end  

X = A\B;
sprintf('a0 = %3.5f\n a1 = %3.5f\n a2 = %3.5f\n a3 = %3.5f\n a4 = %3.5f\n a5 = %3.5f', X(1), X(2), X(3), X(4), X(5), X(6))
end
end

