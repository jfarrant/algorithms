%% FIT TEST DATA
clear
clc
format long

a0 = 0;
a1 = 0;
a2 = -10;
a3 = -30;
a4 = -8.0;
a5 = 7.05;

start_time = 0;
end_time = 3;

Ts = 0.01; % 100 Hz sample freq
t = start_time:Ts:end_time-Ts;
noise_Amplitude = 10;
noise = noise_Amplitude*rand(1, length(t)) - noise_Amplitude/2; % Random noise +/-1
y = a0*t.^0 + a1*t.^1 + a2*t.^2 + a3*t.^3 + a4*t.^4 + a5*t.^5; 
y_noisy = y + noise;

hold on
grid minor
scatter(t, y_noisy)

for i=0:5
X = Fit_Curve(y_noisy, i, Ts);
    if(i == 0)
        estimate = X(1)*t.^0; 
        plot(t, estimate);
    elseif(i == 1)
        estimate = X(1)*t.^0 + X(2)*t.^1; 
        plot(t, estimate);
    elseif(i == 2)
        estimate = X(1)*t.^0 + X(2)*t.^1 + X(3)*t.^2; 
        plot(t, estimate);
    elseif(i == 3)
        estimate = X(1)*t.^0 + X(2)*t.^1 + X(3)*t.^2 + X(4)*t.^3; 
        plot(t, estimate);
    elseif(i == 4)
        estimate = X(1)*t.^0 + X(2)*t.^1 + X(3)*t.^2 + X(4)*t.^3 + X(5)*t.^4; 
        plot(t, estimate);
    elseif(i == 5)
        estimate = X(1)*t.^0 + X(2)*t.^1 + X(3)*t.^2 + X(4)*t.^3 + X(5)*t.^4 + X(6)*t.^5; 
        plot(t, estimate);
    end
end