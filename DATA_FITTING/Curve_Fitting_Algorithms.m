%% Curve fitting
clear
clc
format long
%Variables

% X
X0 = 0;
X1 = 0;
X2 = 0;
X3 = 0;

% A
A_1x1 = 0; A_1x2 = 0; A_1x3 = 0; A_1x4 = 0;
A_2x1 = 0; A_2x2 = 0; A_2x3 = 0; A_2x4 = 0;
A_3x1 = 0; A_3x2 = 0; A_3x3 = 0; A_3x4 = 0;
A_4x1 = 0; A_4x2 = 0; A_4x3 = 0; A_4x4 = 0;

%B
B_1x1 = 0;
B_2x1 = 0;
B_3x1 = 0;
B_4x1 = 0;

%% 3rd Order Polynomial

% X Variable
X = [X0; 
     X1; 
     X2; 
     X3];

A = [A_1x1 A_1x2 A_1x3 A_1x4;
     A_2x1 A_2x2 A_2x3 A_2x4;
     A_3x1 A_3x2 A_3x3 A_3x4;
     A_4x1 A_4x2 A_4x3 A_4x4];
 
B = [B_1x1;
     B_2x1;
     B_3x1;
     B_4x1];
 
 
%% Function to fit
% Actual coefficient to find
a0 = 0.001;
a1 = -15.7;
a2 = 3.3;
a3 = 10.2;

start_time = 0;
end_time = 2;

Ts = 0.01; % 100 Hz sample freq
t = start_time:Ts:end_time-Ts;
noise_Amplitude = 5;
noise = noise_Amplitude*rand(1, length(t)) - noise_Amplitude/2; % Random noise +/-1
y = a0*t.^0 + a1*t.^1 + a2*t.^2 + a3*t.^3; 
y_noisy = y + noise;

figure(1)
hold on
plot(t, y)
scatter(t, y_noisy)
grid minor
xlabel('time (s)')
ylabel('voltage (Volts)')
title('3rd order function')


%% Algorithm for 3rd order polynomial
n = length(t);

for i=1:4
    for j=1:4
        % A(1x1) always = n
        if((i == 1) && (j == 1))
            A(i, j) = n;
        end
        for k=1:n
            % Calculates diagonal, and above and to the right of diagonal,
            % except for A(1x1)
            if((j >= i) && (j ~= 1))
                A(i, j) = A(i, j) + ((k-1)*Ts)^(i + j - 2); 
            % Calculates everything below and to the left of the diagonal
            % by using the fact that the A matrix is symmetric about the
            % Diagonal.
            elseif((i > j) && (i ~= 1))
                A(i, j) = A(j, i); 
            end
            % Calculates B(1x1)
            if((i == 1) && (j == 1))
                B(i, j) = B(i, j) + y_noisy(k);
            % Calculates all other rows of B(ix1)
            elseif((i~=1) && (j==1))
                B(i, j) = B(i, j) + (((k-1)*Ts)^(i-1))*y_noisy(k);
            end
        end
    end
end
%A
%B
X = A\B;
coefficients = sprintf('coefficients of X =\n [%3.5f]\n [%3.5f]\n [%3.5f]\n [%3.5f]', X(1), X(2), X(3), X(4));
coefficients

est = X(1)*t.^0 + X(2)*t.^1 + X(3)*t.^2 + X(4)*t.^3; 
plot(t, est)
plot(t, y-est)

% Curve Fit 0th order
V1 = 13.4*ones(1, length(t));
fit_0 = V1 + noise;
aa_0 = Fit_Curve(fit_0, 0);
aa_0
figure(2)
plot(t, V1, t, fit_0, t, aa_0*ones(1, length(t)))