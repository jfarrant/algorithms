%% CURVE FIT EXAMPLE

voltage = [0.01 0.6 1.1 2.8 3.6];
pitch = [0.3 2.6 12.4 15.7 17];

plot(voltage, pitch)
grid minor
xlabel('voltage')
ylabel('pitch')